$(function(){
    $("[data-toogle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    
    $('.carousel').carousel({
      interval: 3000
    });

    //Query modal
    
    $('#contacto').on('show.bs.modal', function(e){
        console.log('Muestra Modal');

        $('#botones').removeClass('btn-outline-success');
        $('#botones').addClass('btn-outline-primary');
        $('#botones').prop('disabled', true);
    });

    $('#contacto').on('shown.bs.modal', function(e){
      console.log('Se mostro Modal');
    });

    $('#contacto').on('hide.bs.modal', function(e){
      console.log('Modal se oculta');
    });

    $('#contacto').on('hidden.bs.modal', function(e){
      console.log('Modal se oculto');
        $('#botones').prop('disabled', false);
        $('#botones').addClass('btn-outline-success');

    });

  });